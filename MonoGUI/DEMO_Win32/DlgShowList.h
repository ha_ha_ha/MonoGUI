// DlgShowList.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DLGSHOWLIST_H__)
#define __DLGSHOWLIST_H__

class CDlgShowList : public ODialog
{
public:
	CDlgShowList();
	virtual ~CDlgShowList();

	// 初始化
	void Init();

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);
};

#endif // !defined(__DLGSHOWLIST_H__)