#if !defined(AFX_ACCELLLISTDLG_H__60FD8489_A06B_4CE2_BA54_6CAFE11380FB__INCLUDED_)
#define AFX_ACCELLLISTDLG_H__60FD8489_A06B_4CE2_BA54_6CAFE11380FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccellListDlg.h : header file
//
class CEditorView;
class CKeyConfig;

typedef struct _ACCELL
{
	char    psKeyValue[256];
	int     ID;
	struct _ACCELL*	next;
}ACCELL;
/////////////////////////////////////////////////////////////////////////////
// CAccellListDlg dialog

class CAccellListDlg : public CDialog
{
public:
	CEditorView* m_pView;
	int m_iCount;
	struct _ACCELL* m_pAccell;

	// 按键列表
	CKeyConfig* m_pKeyList;

public:
	BOOL RemoveAll ();
	BOOL Open(CString strInfo);					// 从字符串恢复控件属性
	CString Save();								// 将控件属性存入字符串
	BOOL IsValueValid(int nIndex);				// 数值合法性检查

// Construction
public:
	CAccellListDlg(CEditorView* pParent);		// standard constructor

// Dialog Data
	//{{AFX_DATA(CAccellListDlg)
	enum { IDD = IDD_ACCELL };
	CListCtrl	m_lstKeyName;
	CListCtrl	m_lstAccell;
	CEdit		m_edtKeyValue;
	CEdit		m_edtID;
	int			m_nID;
	CString		m_strKeyValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAccellListDlg)
	public:
	virtual int DoModal(int nID);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAccellListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnModify();
	afx_msg void OnDelete();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkListKeyName(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACCELLLISTDLG_H__60FD8489_A06B_4CE2_BA54_6CAFE11380FB__INCLUDED_)
