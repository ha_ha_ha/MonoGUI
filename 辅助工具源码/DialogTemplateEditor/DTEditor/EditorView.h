// EditorView.h : interface of the EditorView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDITORVIEW_H__ED74EF82_A07C_48D9_911B_5ECC7B687AAF__INCLUDED_)
#define AFX_EDITORVIEW_H__ED74EF82_A07C_48D9_911B_5ECC7B687AAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEditorDoc;
class CCtrlMgt;
class CAttributeDlg;
class CAccellListDlg;

class CEditorView : public CView
{
public:
	enum {
		CURSOR_NONE = 0,
		CURSOR_MOVE,
		CURSOR_U_D,
		CURSOR_L_R
	};

public:
	CDC m_MemDC;
	CCtrlMgt* m_pCtrlMgt;
	CAttributeDlg* m_pAttributeDlg;
	CAccellListDlg* m_pAccellListDlg;

	// 按键映射表的文件路径
	char m_KeyMapPath[256];

private:
	BOOL m_bAssistLine;		// 辅助线显示标志
	CPoint m_ptMousePointer;// 鼠标箭头的位置
	BOOL m_bModified;		// 修改标志

public:
	void SetModified();		// 设置修改标志
	void ClrModified();		// 清除修改标志
	BOOL IsModified();		// 得到修改标志
	void Display();			// 刷新屏幕的显示
	// 绘制辅助线
	void DrawAssistLine(CDC* pdc);

private:
	BOOL m_bPick;			// 控件被选中标志
	int  m_iCursor;			// 显示什么样的光标（1、移动；2、上下；3：作右）
	HCURSOR m_hSizeAll;		// 指向四个方向的箭头
	HCURSOR m_hSizeNS;		// 指向上下的双向箭头
	HCURSOR m_hSizeWE;		// 指向左右的双向箭头
	HCURSOR m_hArrow;		// 原始的鼠标箭头

	void SetCurCursor();	// 根据m_iCursor参数进行光标显示

protected: // create from serialization only
	CEditorView();
	DECLARE_DYNCREATE(CEditorView)

// Attributes
public:
	CEditorDoc* GetDocument();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditorView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void SwitchAssistLine();

// Generated message map functions
protected:
	//{{AFX_MSG(CEditorView)
	afx_msg void OnAddStatic();
	afx_msg void OnAddButton();
	afx_msg void OnAddEdit();
	afx_msg void OnAddList();
	afx_msg void OnAddCombo();
	afx_msg void OnAddProgress();
	afx_msg void OnEditAccell();
	afx_msg void OnDelete();
	afx_msg void OnSettab();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMoveUp();
	afx_msg void OnMoveDown();
	afx_msg void OnMoveLeft();
	afx_msg void OnMoveRight();
	afx_msg void OnResizeUp();
	afx_msg void OnResizeDown();
	afx_msg void OnResizeLeft();
	afx_msg void OnResizeRight();
	afx_msg void OnAddImageButton();
	afx_msg void OnAddCheckBox();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // debug version in EditorDoc.cpp
inline CEditorDoc* CEditorView::GetDocument()
   { return (CEditorDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITORVIEW_H__ED74EF82_A07C_48D9_911B_5ECC7B687AAF__INCLUDED_)
